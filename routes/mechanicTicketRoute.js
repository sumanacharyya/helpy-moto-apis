const express = require("express");
const {
  createTicket,
  getSingleTicket,
  updateSingleTicket,
  getAllTickets,
  deleteSingleTicket,
} = require("../controllers/mechanicTicketController");

const { isLoggedIn, customRole } = require("../middlewares/user");

const router = express.Router();

router.route("/ticket/mechanic/create").post(createTicket);
// router.route("/ticket/create").post(isLoggedIn, createTicket);
router.route("/ticket/mechanic/:id").get(getSingleTicket);
// router.route("/ticket/:id").get(isLoggedIn, getSingleTicket);
router.route("/ticket/mechanic/update/:id").put(updateSingleTicket);
// router.route("/ticket/update/:id").put(isLoggedIn, updateSingleTicket);
router.route("/ticket/mechanic/delete/:id").delete(deleteSingleTicket);
// router.route("/ticket/delete/:id").delete(isLoggedIn, deleteSingleTicket);
router.route("/allticket/mechanic").get(getAllTickets);

// Admin only routes
// router.route("/admin/tickets").get(isLoggedIn, adminGetAllTickets);
// router
//   .route("/admin/ticket/update/:id")
//   .put(isLoggedIn, customRole("admin"), adminUpdateATicket);
// router
//   .route("/admin/ticket/delete/:id")
//   .delete(isLoggedIn, customRole("admin"), adminDeleetATicket);

module.exports = router;
