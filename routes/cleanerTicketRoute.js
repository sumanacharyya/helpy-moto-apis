const express = require("express");

const {
  createTicket,
  getSingleTicket,
  getAllTickets,
  updateSingleTicket,
  deleteSingleTicket,
} = require("../controllers/cleanerTicketController");
const { isLoggedIn } = require("../middlewares/cleanerUser");

const router = express.Router();

router.route("/ticket/cleaner/create").post(createTicket);
// router.route("/ticket/create").post(isLoggedIn, createTicket);
router.route("/ticket/cleaner/:id").get(getSingleTicket);
// router.route("/ticket/:id").get(isLoggedIn, getSingleTicket);
router.route("/ticket/cleaner/update/:id").put(updateSingleTicket);
// router.route("/ticket/update/:id").put(isLoggedIn, updateSingleTicket);
router.route("/ticket/cleaner/delete/:id").delete(deleteSingleTicket);
// router.route("/ticket/delete/:id").delete(isLoggedIn, deleteSingleTicket);
router.route("/allticket/cleaner").get(getAllTickets);

module.exports = router;
