const express = require("express");
const {
  signupDriver,
  loginDriver,
  logoutDriver,
  getSingleDriver,
  updateSingleDriver,
  deleteSingleDriver,
  getAllDriver,
  getLoggedinDriverDetails,
  resetDriverPassword,
  forgotDriverPassword,
} = require("../controllers/driverController");

const { isLoggedIn } = require("../middlewares/driverUser");

const router = express.Router();

router.route("/driver/signup").post(signupDriver);
router.route("/driver/login").post(loginDriver);
router.route("/driver/logout").get(logoutDriver);
router.route("/driver/forgotpassword").post(forgotDriverPassword);
router.route("/driver/password/reset/:token").post(resetDriverPassword);
router.route("/driverdashboard").get(isLoggedIn, getLoggedinDriverDetails);
router.route("/driver/update/:id").put(isLoggedIn, updateSingleDriver);
router.route("/driver/delete/:id").delete(isLoggedIn, deleteSingleDriver);
router.route("/driver/:id").get(isLoggedIn, getSingleDriver);
router.route("/drivers").get(getAllDriver);

module.exports = router;
