const express = require("express");
const {
  getSingleMechanic,
  deleteSingleMechanic,
  updateSingleMechanic,
  getAllMechanics,
  signupMechanic,
  loginMechanic,
  logoutMechanic,
  getLoggedinMechanicDetails,
  resetMechanicPassword,
  forgotMechanicPassword,
} = require("../controllers/mechanicController");
const { isLoggedIn } = require("../middlewares/mechanicUser");

const router = express.Router();

router.route("/mechanic/signup").post(signupMechanic);
router.route("/mechanic/login").post(loginMechanic);
router.route("/mechanic/logout").get(logoutMechanic);
router.route("/mechanic/forgotpassword").post(forgotMechanicPassword);
router.route("/mechanic/password/reset/:token").post(resetMechanicPassword);
router.route("/mechanicdashboard").get(isLoggedIn, getLoggedinMechanicDetails);
router.route("/mechanic/update/:id").put(isLoggedIn, updateSingleMechanic);
router.route("/mechanic/delete/:id").delete(isLoggedIn, deleteSingleMechanic);
router.route("/mechanic/:id").get(isLoggedIn, getSingleMechanic);
router.route("/mechanics").get(getAllMechanics);

module.exports = router;
