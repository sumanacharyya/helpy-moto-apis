const express = require("express");

const {
  signupCleaner,
  loginCleaner,
  logoutCleaner,
  getSingleCleaner,
  updateSingleCleaner,
  deleteSingleCleaner,
  getLoggedinCleanerDetails,
  getAllCleaners,
  resetCleanerPassword,
  forgotCleanerPassword,
} = require("../controllers/cleanerController");
const { isLoggedIn } = require("../middlewares/cleanerUser");

const router = express.Router();

router.route("/cleaner/signup").post(signupCleaner);
router.route("/cleaner/login").post(loginCleaner);
router.route("/cleaner/logout").get(logoutCleaner);
router.route("/cleaner/forgotpassword").post(forgotCleanerPassword);
router.route("/cleaner/password/reset/:token").post(resetCleanerPassword);
router.route("/cleanerdashboard").get(isLoggedIn, getLoggedinCleanerDetails);
router.route("/cleaner/update/:id").put(isLoggedIn, updateSingleCleaner);
router.route("/cleaner/delete/:id").delete(isLoggedIn, deleteSingleCleaner);
router.route("/cleaner/:id").get(isLoggedIn, getSingleCleaner);
router.route("/cleaners").get(getAllCleaners);

module.exports = router;
